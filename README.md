# Fleat : Antique Shop game project

[![Figma Link](https://img.shields.io/badge/design_board-blue?style=for-the-badge&logo=figma&logoColor=white&link)](https://www.figma.com/file/V6XCieeK6A5zRpPao6ZMZF/Alpagames?type=whiteboard&node-id=0%3A1&t=O9YU9qYiPjMKov1I-1)

## Dependencies 

* Godot >= 4.0.2
    * Blender Helper
    * Dialogue Manager
    * Extra Export Hints
    * GDShell
    * Godot Git Plugin
    * Resources Spreadsheet View
    * Todo Manager

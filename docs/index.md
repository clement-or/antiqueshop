# Documentation

Documenter le fonctionnement interne du jeu. Cette documentation sert à expliquer comment setup certaines scènes, comment fonctionnent certains éléments de jeu en interne.
Elle est à destination des personnes qui vont modifier les scènes et créer du contenu.
Pas de documentation du gameplay : la doc c'est le jeu. 
# Represents a swipe input done on a Touch screen. The difference with a ScreenDrag is that a Swipe is a single motion with an end
class_name InputEventScreenSwipe
extends Resource

const NOT_INITIALIZED = -1

var min_distance_between_events_px := 25	# Minimum distance between two drag events for them to be registered as separate
var max_time_between_events_ms := 500.0		# If there is no new event after this time, capturing stops

var position := Vector2.ZERO                # Position from which the event started
var direction := Vector2.ZERO				# The average direction of the swipe
var direction_name: GameInput.Directions	# Closest 4-axis direction
var velocity := Vector2.ZERO				# The average velocity of the swipe
var duration_ms := -1.0						# The total duration of the swipe
var index := -1								# The index of the swipe event (depends on how many fingers are touching the screen)
var consumed := false                       # Has this event already been consumed

var _is_capturing := false
var _captured_events: Array[InputEventScreenDrag]
var _start_time_ms := -1.0
var _prev_event_time_ms := -1.0

func process_capture(event: InputEventScreenDrag, cur_time_ms: float):
	if index == NOT_INITIALIZED:
		index = event.index
	elif index != event.index:
		return
	
	var something_got_captured = _captured_events.size() > 0
	if not _is_capturing and not something_got_captured:
		_is_capturing = true
		_start_time_ms = cur_time_ms
		_prev_event_time_ms = cur_time_ms
	
	if not _is_capturing: return
	
	var new_event_is_too_close = _captured_events.size() and _captured_events[-1].position.distance_squared_to(event.position) < pow(min_distance_between_events_px, 2)
	if not something_got_captured or not new_event_is_too_close:
		_captured_events.push_back(event)
		_prev_event_time_ms = cur_time_ms

	# End capture
	var time_since_last_event = cur_time_ms - _prev_event_time_ms
	if time_since_last_event > max_time_between_events_ms:
		_end_capture()
		return

func process_release(event: InputEventScreenTouch, cur_time_ms: float):
	if not event.pressed and event.index == index and _captured_events.size():
		_end_capture(cur_time_ms)

func has_finished_capturing() -> bool:
	var ret = not _is_capturing and _captured_events.size()
	return ret

func _end_capture(cur_time_ms := -1.0):
	_is_capturing = false
	position = _captured_events[0].position

	for e in _captured_events:
		velocity += e.velocity
		direction += e.relative
	velocity /= _captured_events.size()
	direction /= _captured_events.size()
	direction = direction.normalized()
	direction_name = GameInput.vec_to_direction(direction)
	duration_ms = cur_time_ms - _start_time_ms if cur_time_ms > -1.0 else 0.0

func _to_string():
	var ret = "%s { \"index\": %d\n, \"position\": %s\n, \"dir_name\": %s ,\"direction\": %s\n, \"velocity\": %s }" % [resource_name, index ,str(position), str(direction_name), str(direction), str(velocity)]
	return ret
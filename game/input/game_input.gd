extends Node

signal input_swipe(event: InputEventScreenSwipe)

enum Directions {
	Up 		= 	0b0001,
	Down 	= 	0b0010,
	Right 	= 	0b0100,
	Left 	= 	0b1000
}

var _vec_to_dir_map = {
	Vector2.UP: Directions.Up,
	Vector2.DOWN: Directions.Down,
	Vector2.RIGHT: Directions.Right,
	Vector2.LEFT: Directions.Left,
}

@export var swipe_settings: Dictionary = {
	"max_time_between_dragevents_ms" = 200.0,
	"max_swipe_duration_ms" = 1000.0,
	"min_distance_between_dragevents_px" = 100,
	"swipe_events_history" = 5
}

var _swipe_history: Array[InputEventScreenSwipe]

func _input(_event):
	if _event is InputEventScreenDrag:

		var history_is_empty = _swipe_history.size() <= 0
		if history_is_empty or _swipe_history[-1].has_finished_capturing():
			
			var swipe_event = InputEventScreenSwipe.new()
			swipe_event.max_time_between_events_ms = swipe_settings.max_time_between_dragevents_ms
			#swipe_event.max_swipe_duration_ms = swipe_settings.max_swipe_duration_ms
			swipe_event.min_distance_between_events_px = swipe_settings.min_distance_between_dragevents_px
			_swipe_history.push_back(swipe_event)

		for e in _swipe_history:
			if not e.has_finished_capturing():
				e.process_capture(_event, Time.get_ticks_msec())
				if e.has_finished_capturing() and not e.consumed:
					emit_signal("input_swipe", e)
		
		var history_too_large = _swipe_history.size() > swipe_settings.swipe_events_history
		var swipe_settings_are_dumb = swipe_settings.swipe_events_history <= 0
		if not swipe_settings_are_dumb and history_too_large:
			for i in range(swipe_settings.swipe_events_history):
				if _swipe_history[0].has_finished_capturing():
					_swipe_history.pop_front()
		if swipe_settings_are_dumb:
			push_warning("GameInput.gd - Swipe settings history parameter is dumb, swipe history will never be flushed.")

	if _event is InputEventScreenTouch:
		for e in _swipe_history:
			e.process_release(_event, Time.get_ticks_msec())
			if e.has_finished_capturing() and not e.consumed:
				emit_signal("input_swipe", e)


# TOFIX: Improve this
func vec_to_direction(vec: Vector2) -> Directions:
	vec = vec.normalized()
	var closest = Vector2.ZERO
	var prev_dot = 0

	for v in [Vector2.UP, Vector2.RIGHT, Vector2.DOWN, Vector2.LEFT]:
		if v.dot(vec) > prev_dot:
			prev_dot = v.dot(vec)
			closest = v
	
	return _vec_to_dir_map[closest]